import edu.ro.utcn.Bank.Bank;
import edu.ro.utcn.Bank.Client.Account.Account;
import edu.ro.utcn.Bank.Client.Person;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class BankTest {

    private Bank bank = new Bank();

    @Test
    public void testAddAccount(){
        Person person = new Person(5,"Nume","Adresa");
        Account account = new Account("RO2940506234543",20);
        bank.addPerson(person);
        int size = bank.getBank().get(person).size();
        assertTrue(size == 0);
        bank.addAccount(person,account);
        int size2 = bank.getBank().get(person).size();
        assertTrue(size+1==size2);
    }

    @Test
    public void testRemoveAccount(){
        Person person = new Person(5,"Nume","Adresa");
        Account account = new Account("RO2940506234543",20);
        bank.addPerson(person);
        int size = bank.getBank().get(person).size();
        assertTrue(size == 0);
        bank.addAccount(person,account);
        bank.removeAccount(account.getIBAN());
        int size2 = bank.getBank().get(person).size();
        assertTrue(size==size2);
    }

    @Test
    public void testDeposit(){
        Person person = new Person(5,"Nume","Adresa");
        Account account = new Account("RO2940506234543",20);
        bank.addPerson(person);
        int size = bank.getBank().get(person).size();
        assertTrue(size == 0);
        bank.addAccount(person,account);
        bank.writeAccount(account.getIBAN(),100,true);
        boolean test = (account.getAmount() == 120);
        assertTrue(test);
    }

    @Test
    public void testWithdraw(){
        Person person = new Person(5,"Nume","Adresa");
        Account account = new Account("RO2940506234543",20);
        bank.addPerson(person);
        int size = bank.getBank().get(person).size();
        assertTrue(size == 0);
        bank.addAccount(person,account);
        bank.writeAccount(account.getIBAN(),10,false);
        boolean test = (account.getAmount() == 10);
        assertTrue(test);
    }
}
