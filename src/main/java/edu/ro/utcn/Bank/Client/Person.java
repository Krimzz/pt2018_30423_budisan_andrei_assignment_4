package edu.ro.utcn.Bank.Client;

import edu.ro.utcn.Bank.Client.Account.Account;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer {
    private int id;
    private String name;
    private String address;

    public Person() {

    }

    public Person(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return getId() == person.getId() &&
                Objects.equals(getName(), person.getName());
    }

    @Override
    public int hashCode() {
        StringBuilder hash = new StringBuilder(getId()+getName()+getAddress());
        return hash.toString().hashCode();
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public void update(Observable o, Object arg) {
        Account a = (Account)o;
        if(a.getAmount()>0.0){
            System.out.println("Person " + this.name +" account modified " + a.toString());
        }else {
            System.out.println("Person " + this.name +" account deleted " + a.toString());
        }
    }
}
