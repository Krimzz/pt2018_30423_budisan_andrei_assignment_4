package edu.ro.utcn.Bank.Client.Account;

public class SavingAccount extends Account {
    private static double extrafee = 0.02;
    private int numberOfFreeTransactions;

    public SavingAccount(String IBAN,double amount){
        super(IBAN,amount);
        numberOfFreeTransactions = 10;
        setType("Saving Account");
    }

    @Override
    public void deposit(double amount) {
        super.deposit(amount);
    }

    @Override
    public double withdraw(double amount) {
        if(numberOfFreeTransactions > 0){
            numberOfFreeTransactions--;
            return super.withdraw(amount);
        }else {
            if (amount + extrafee * amount < getAmount()) {
                return super.withdraw(amount) + extrafee * amount;
            }
        }
        return -1;
    }
}
