package edu.ro.utcn.Bank.Client.Account;

import java.util.Objects;
import java.util.Observable;

public class Account extends Observable {
    private String IBAN;
    private double amount;
    private String type;

    public Account() {

    }

    public Account(String IBAN, double amount) {
        this.IBAN = IBAN;
        this.amount = amount;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(getIBAN(), account.getIBAN());
    }

    @Override
    public int hashCode() {
        StringBuilder hash = new StringBuilder(getIBAN());
        return hash.toString().hashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "IBAN='" + IBAN + '\'' +
                ", amount=" + amount +
                '}';
    }

    public void deposit(double amount) {
        this.amount = this.amount + amount;
        setChanged();
        notifyObservers();
    }

    public double withdraw(double amount) {
        if (this.amount < amount) {
            return -1;
        } else {
            this.amount = this.amount - amount;
            setChanged();
            notifyObservers();
            return amount;
        }
    }

    public void notifyDelete(){
        setChanged();
        notifyObservers();
    }
}
