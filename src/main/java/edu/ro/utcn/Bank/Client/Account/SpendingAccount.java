package edu.ro.utcn.Bank.Client.Account;

public class SpendingAccount extends Account {
    private double spent;

    public SpendingAccount(String IBAN,double amount){
        super(IBAN,amount);
        spent = 0;
        setType("Spending Account");
    }

    @Override
    public void deposit(double amount) {
        super.deposit(amount);
    }

    @Override
    public double withdraw(double amount) {
        spent = spent + amount;
        return super.withdraw(amount);
    }

    public double getSpent() {
        return spent;
    }

    public void setSpent(double spent) {
        this.spent = spent;
    }
}
