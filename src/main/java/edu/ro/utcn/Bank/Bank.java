package edu.ro.utcn.Bank;

import edu.ro.utcn.Bank.Client.Account.Account;
import edu.ro.utcn.Bank.Client.Person;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @invariant isWellFormed()
 */

public class Bank implements BankProc {
    private HashMap<Person, Set<Account>> bank;

    public Bank() {
        bank = new HashMap<>();
    }

    public HashMap<Person, Set<Account>> getBank() {
        return bank;
    }

    public void setBank(HashMap<Person, Set<Account>> bank) {
        this.bank = bank;
    }

    @Override
    public void addAccount(Person person, Account account) {
        assert account != null : "account is null";

        Account a = readAccount(account.getIBAN());

        assert a == null : "account with same name found at person";

        Set<Account> accounts = bank.get(person);
        int size = bank.get(person).size();
        accounts.add(account);
        account.addObserver(person);
        assert size + 1 == accounts.size() : "account not added to client";
    }

    @Override
    public void removeAccount(String IBAN) {
        assert !bank.isEmpty() : "no clients in bank to be removed";
        boolean done = false;

        Set<Person> persons = bank.keySet();
        Set<Account> removed = new HashSet<>();

        Person person = new Person();
        for (Person p : persons) {
            Set<Account> accounts = bank.get(p);
            for (Account a : accounts) {
                if (a.getIBAN().compareTo(IBAN) == 0) {
                    done = true;
                    removed.add(a);
                    a.setAmount(-1);
                    a.notifyDelete();
                    person = p;
                }
            }
        }

        assert done : "account not found at this person";
        Set<Account> removing = bank.get(person);
        int size = removing.size();
        removing.removeAll(removed);
        assert removing.size() == size - removed.size() : "account not removed";
    }

    @Override
    public Account readAccount(String IBAN) {
        assert !bank.isEmpty() : "no clients in bank to be removed";
        assert isWellFormed();
        Set<Person> persons = bank.keySet();
        for (Person p : persons) {
            for (Account a : bank.get(p)) {
                if (a.getIBAN().compareTo(IBAN) == 0) {
                    return a;
                }
            }
        }
        return null;
    }

    @Override
    public void writeAccount(String IBAN, double amount, boolean operation) {

        Account a = readAccount(IBAN);
        assert a != null : "account not found";
        assert isWellFormed();

        if (operation) { //deposit on true
            a.deposit(amount);
        } else {
            a.withdraw(amount); //withdraw on false
        }
    }

    @Override
    public void addPerson(Person person) {
        assert person != null : "person is null";
        assert person.getId() != 0 : "no id found in this person";
        assert person.getAddress() != null : "no address found in this person";
        assert !bank.containsKey(person) : "person already in bank";
        HashSet<Account> accounts = new HashSet<>();
        int size = bank.size();
        bank.put(person, accounts);
        assert size + 1 == bank.size() : "person was not added into the bank";
    }

    @Override
    public void removePerson(Person person) {
        assert person != null : "person is null";
        assert !bank.isEmpty() : "no clients in bank to be removed";
        assert bank.containsKey(person) : "no references of this person found in the bank";
        int size = bank.size();
        bank.remove(person);
        assert size - 1 == bank.size() : "person was not removed";
    }

    private boolean isWellFormed() {
        if (bank.size() >= 0) {
            return true;
        } else {
            return false;
        }
    }

}
