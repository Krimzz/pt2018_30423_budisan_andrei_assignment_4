package edu.ro.utcn.Bank;

import edu.ro.utcn.Bank.Client.Account.Account;
import edu.ro.utcn.Bank.Client.Person;

public interface BankProc {
    /**
     * Add a new account to the bank
     * @pre account != NULL
     * @post size() == size()@pre + 1
     */
    void addAccount(Person person, Account account);

    /**
     * @pre bank.isEmpty()
     * @post account exist
     * @post removing.size() == size - removed.size()
     */
    void removeAccount(String IBAN);

    /**
     * @pre bank.isEmpty()
     * @pre isWellFormed();
     */
    Account readAccount(String IBAN);

    /**
     * @pre a != null : "account not found";
     * @pre isWellFormed();
     */
    void writeAccount(String IBAN,double amount,boolean operation);

    /**
     * @pre person != null
     * @pre person.getId() != 0
     * @pre person.getAddress() != null
     * @pre !bank.containsKey(person)
     * @post size + 1 == bank.size()
     */
    void addPerson(Person person);

    /**
     * @pre person != null
     * @pre !bank.isEmpty()
     * @pre bank.containsKey(person)
     * @post size - 1 == bank.size()
     */
    void removePerson(Person person);
}
