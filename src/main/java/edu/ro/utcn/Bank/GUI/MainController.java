package edu.ro.utcn.Bank.GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import edu.ro.utcn.Bank.Bank;
import edu.ro.utcn.Bank.Client.Account.Account;
import edu.ro.utcn.Bank.Client.Account.SavingAccount;
import edu.ro.utcn.Bank.Client.Account.SpendingAccount;
import edu.ro.utcn.Bank.Client.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

public class MainController implements Initializable {
    @FXML
    private JFXTextField clientID;

    @FXML
    private JFXTextField clientName;

    @FXML
    private JFXTextField clientAddress;

    @FXML
    private JFXTextField accountIBAN;

    @FXML
    private JFXTextField accountAmount;

    @FXML
    private JFXTextField operationAccountIBAN;

    @FXML
    private JFXTextField operationAmount;

    @FXML
    private JFXTextField accountID;

    @FXML
    private JFXTextField accountName;

    @FXML
    private JFXButton clientAdd;

    @FXML
    private JFXButton clientRemove;

    @FXML
    private JFXButton accountAdd;

    @FXML
    private JFXButton accountRemove;

    @FXML
    private JFXButton Transaction;

    @FXML
    private TableView<Person> clientTable;

    @FXML
    private TableColumn<Person, Integer> clientIDC;

    @FXML
    private TableColumn<Person, String> clientNameC;

    @FXML
    private TableColumn<Person, String> clientAddressC;

    @FXML
    private TableView<Account> accountTable;

    @FXML
    private TableColumn<Account, String> accountIBANC;

    @FXML
    private TableColumn<Account, Double> accountAmountC;

    @FXML
    private TableColumn<Account, Integer> accountTypeC;

    @FXML
    private JFXToggleButton typeAccount;

    @FXML
    private JFXToggleButton transactionType;

    private Bank bank = new Bank();

    @FXML
    public void handleButtonAction(MouseEvent event) {

        if (event.getSource() == clientAdd) {
            Person person = new Person(Integer.parseInt(clientID.getText()), clientName.getText(), clientAddress.getText());
            bank.addPerson(person);
            updateClientTable();
        }

        if (event.getSource() == clientRemove) {
            Person person = new Person(Integer.parseInt(clientID.getText()), clientName.getText(), clientAddress.getText());
            bank.removePerson(person);
            updateClientTable();
            updateAccountTable();
        }

        if (event.getSource() == accountAdd) {
            Set<Person> persons = bank.getBank().keySet();
            if (typeAccount.getText().compareTo("Saving") == 0) {
                SavingAccount savingAccount = new SavingAccount(accountIBAN.getText(), Double.parseDouble(accountAmount.getText()));
                for (Person p : persons) {
                    if (p.getId() == Integer.parseInt(accountID.getText()) && p.getName().compareTo(accountName.getText()) == 0) {
                        bank.addAccount(p, savingAccount);
                    }
                }
            } else {
                SpendingAccount spendingAccount = new SpendingAccount(accountIBAN.getText(), Double.parseDouble(accountAmount.getText()));
                for (Person p : persons) {
                    if (p.getId() == Integer.parseInt(accountID.getText()) && p.getName().compareTo(accountName.getText()) == 0) {
                        bank.addAccount(p, spendingAccount);
                    }
                }
            }
            updateAccountTable();
        }

        if (event.getSource() == accountRemove) {
            bank.removeAccount(accountIBAN.getText());
            updateAccountTable();
        }

        if (event.getSource() == Transaction) {
            if (transactionType.getText().compareTo("Withdraw") == 0) {
                bank.writeAccount(operationAccountIBAN.getText(), Double.parseDouble(operationAmount.getText()), false);
            } else {
                bank.writeAccount(operationAccountIBAN.getText(), Double.parseDouble(operationAmount.getText()), true);
            }
            updateAccountTable();
        }

        if (event.getSource() == typeAccount) {
            if (typeAccount.isSelected()) {
                typeAccount.setText("Spending");
            } else {
                typeAccount.setText("Saving");
            }
        }

        if (event.getSource() == transactionType) {
            if (transactionType.isSelected()) {
                transactionType.setText("Deposit");
            } else {
                transactionType.setText("Withdraw");
            }
        }

    }

    private ObservableList<Person> clients;
    private ObservableList<Account> accounts;


    public void updateClientTable() {
        Set<Person> persons = bank.getBank().keySet();
        clients = FXCollections.observableArrayList();
        clients.addAll(persons);

        clientIDC.setCellValueFactory(new PropertyValueFactory<Person, Integer>("id"));
        clientNameC.setCellValueFactory(new PropertyValueFactory<Person, String>("name"));
        clientAddressC.setCellValueFactory(new PropertyValueFactory<Person, String>("address"));

        clientTable.setItems(null);
        clientTable.setItems(clients);
        clientTable.refresh();
    }

    public void updateAccountTable() {
        Set<Person> persons = bank.getBank().keySet();
        accounts = FXCollections.observableArrayList();
        for (Person p : persons) {
            Set<Account> acc = bank.getBank().get(p);
            accounts.addAll(acc);
        }

        accountIBANC.setCellValueFactory(new PropertyValueFactory<Account,String>("IBAN"));
        accountAmountC.setCellValueFactory(new PropertyValueFactory<Account,Double>("amount"));
        accountTypeC.setCellValueFactory(new PropertyValueFactory<Account,Integer>("type"));

        accountTable.setItems(null);
        accountTable.setItems(accounts);
        accountTable.refresh();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
